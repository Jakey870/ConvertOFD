package com.ofd.controller;

import org.dom4j.DocumentException;
import com.ofd.service.ConvertOfdService;
import com.ofd.soft.xml.bean.OfdXmlBean;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;

public class IndexController extends Controller {

	@Inject
	ConvertOfdService convertOfdService;

	public void getOfdData() throws DocumentException {
		renderJson(convertOfdService.getOfdData(new OfdXmlBean(), "政务服务事项电子文件归档规范.ofd"));
	}

}
