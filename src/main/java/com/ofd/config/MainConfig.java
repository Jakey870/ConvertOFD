package com.ofd.config;

import com.ofd.controller.IndexController;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.proxy.CglibProxyFactory;
import com.jfinal.json.JFinalJson;
import com.jfinal.json.MixedJsonFactory;
import com.jfinal.kit.PropKit;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;

public class MainConfig extends JFinalConfig {
	/**
	 * 将全局配置提出来 方便其他地方重用
	 */

	/**
	 * 配置JFinal常量
	 */
	@Override
	public void configConstant(Constants me) {
	    PropKit.use("AppConfig.properties");
		me.setInjectDependency(true);
		me.setDevMode(PropKit.getBoolean("DevMode", true));
		me.setMaxPostSize(Integer.MAX_VALUE);
		me.setJsonFactory(MixedJsonFactory.me());
		JFinalJson.setDefaultConvertDepth(30);
		me.setProxyFactory(new CglibProxyFactory());
		me.setJsonFactory(new MixedJsonFactory());
	}

	/**
	 * 配置项目路由 路由拆分到 FrontRutes 与 AdminRoutes 之中配置的好处： 1：可分别配置不同的 baseViewPath 与
	 * Interceptor 2：避免多人协同开发时，频繁修改此文件带来的版本冲突 3：避免本文件中内容过多，拆分后可读性增强 4：便于分模块管理路由
	 */
	@Override
	public void configRoute(Routes me) {
		me.add("/", IndexController.class);
	}

	/**
	 * 配置JFinal插件 数据库连接池 ActiveRecordPlugin 缓存 定时任务 自定义插件
	 */
	@Override
	public void configPlugin(Plugins me) {
//		String url = PropKit.get("jdbc.url");
//		String username = PropKit.get("jdbc.username");
//		String password = PropKit.get("jdbc.password");
//		String driverClass = PropKit.get("jdbc.driver");
//		DruidPlugin dbPlugin = new DruidPlugin(url, username, password, driverClass);
//		ActiveRecordPlugin arp = new ActiveRecordPlugin(dbPlugin);
//		me.add(dbPlugin);
//		me.add(arp);
	}

	/**
	 * 配置全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {
	}

	/**
	 * 配置全局处理器
	 */
	@Override
	public void configHandler(Handlers me) {
		// 说明：druid的统计页面涉及安全性 需要自行处理根据登录权限判断是否能访问统计页面
		// me.add(DruidKit.getDruidStatViewHandler()); // druid 统计页面功能
	}

	/**
	 * 项目启动后调用
	 */
	@Override
	public void onStart() {
	}

	/**
	 * 配置模板引擎
	 */
	@Override
	public void configEngine(Engine me) {
	}

	public static void main(String[] args) {
		UndertowServer.create(MainConfig.class, "undertow.properties").start();
	}

}
