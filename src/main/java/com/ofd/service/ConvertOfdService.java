package com.ofd.service;

import java.awt.Font;
import org.dom4j.DocumentException;
import com.ofd.soft.util.Loadfont;
import com.ofd.soft.xml.ReadXML;
import com.ofd.soft.xml.bean.OfdXmlBean;

public class ConvertOfdService {

	/**
	 * 解析ofd
	 *@Time:2020年4月11日 - 下午8:41:38
	 * @auto:李德才
	 * @param: @param ofdXmlBean
	 * @param: @param fileName
	 * @param: @return
	 * @param: @throws DocumentException      
	 * @return: OfdXmlBean      
	 * @throws
	 */
	public OfdXmlBean getOfdData(OfdXmlBean ofdXmlBean, String fileName) throws DocumentException {
//		String folderPath = PathKit.getWebRootPath().replace("webapp", "resources") + File.separator + "demoFolder";
		String folderPath = "D:\\demoFolder";
		return ReadXML.ofd2H5(new OfdXmlBean(), folderPath, fileName);
	}
	
	
	/**
	 * 获取字体文件名称
	 *@Time:2020年4月11日 - 下午8:41:29
	 * @auto:李德才
	 * @param: @param filePath
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	public String getFontName(String filePath) {
		Font loadFont = Loadfont.loadFont(filePath);
		return loadFont.getFontName();
	}

}
