package com.ofd.soft.xml.bean;

import java.util.List;
import lombok.Data;

/**
 * 页面内容
 * 
 * @ClassName: ContentXmlBean
 * @Description:TODO(描述这个类的作用)
 * @author: 李德才
 * @date: 2020年4月5日 下午1:39:25
 */
@Data
public class ContentXmlBean {
	private List<physicalBoxXmlBean> physicalBoxList;
	private List<LayerBean> layerList;
	private String path;
}
