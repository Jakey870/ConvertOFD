package com.ofd.soft.xml.bean;


import lombok.Data;

/**
 * 文档资源对象，多为图片
 * 
 * @ClassName: MultiMediaBean
 * @Description:TODO(描述这个类的作用)
 * @author: 李德才
 * @date: 2020年4月18日 下午1:08:44
 */

@Data
public class MultiMediaBean {
//	资源类型
	private String type;
//	资源路径
	private String mediaFile;
//	标签ID
	private String id;
//	图片的Base64流
	private String base64;

}
