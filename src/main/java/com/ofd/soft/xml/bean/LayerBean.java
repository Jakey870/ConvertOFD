package com.ofd.soft.xml.bean;

import java.util.List;

import lombok.Data;

/**
 * 文档分层，目前了解上层为文字，下层为图片
 * @ClassName:  LayerBean   
 * @Description:TODO(描述这个类的作用)   
 * @author: 李德才
 * @date:   2020年4月18日 下午1:12:18
 */


@Data
public class LayerBean {
	private List<TextObjectBean> textObjectBeanList;
	private List<ImageObjectBean> imageObjectBeanList;
	private List<PathObjectBean> pathObjectBeanList;
}
