package com.ofd.soft.xml.bean;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class OfdXmlBean {
	
//	指向文档的根节点文档
	private  DocumentXmlBean  docRoots ;
//	作者
	private  String author ;
//	主题
	private String subject;
//	摘要与注释
	private String ofdAbstract;
//	创建日期
	private String creationDate;
//	修改日期
	private String modDate;
//	文档UUID
	private String docInfo;
	
	

}
